<html>
<head>
  <title>Insert to MySQL</title>
  <style>
    table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; }
    td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; }
    tr:nth-child(even) { background-color: #dddddd; }
  </style>
</head>

<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="javax.naming.*" %>

<body>
<%
String firstName = request.getParameter("first");
String lastName = request.getParameter("last");

String serverIP = request.getLocalAddr();

if( firstName == null || lastName == null ) {
    response.sendRedirect("index.jsp");
} else {

    int randomNum = 0 + (int)(Math.random() * 999999);

    DataSource pool;

    InitialContext ctx = new InitialContext();
    pool = (DataSource)ctx.lookup("java:comp/env/jdbc/TestDB");

    Connection conn = null;
    Statement  stmt = null;

    conn = pool.getConnection();
    stmt = conn.createStatement();

    String query = " insert into employees (id, first_name, last_name) values (?, ?, ?)";
    PreparedStatement preparedStmt = conn.prepareStatement(query);
    preparedStmt.setInt (1, randomNum);
    preparedStmt.setString (2, firstName);
    preparedStmt.setString (3, lastName);
    preparedStmt.execute();
    conn.close();
    response.sendRedirect("index.jsp");
}
%>
</body>
</html>