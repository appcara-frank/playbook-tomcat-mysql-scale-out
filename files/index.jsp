<html>
<head>
  <title>Test</title>
  <style>
    table { font-family: arial, sans-serif; border-collapse: collapse; width: 100%; }
    td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; }
    tr:nth-child(even) { background-color: #dddddd; }
  </style>
</head>

<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ page import="javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="javax.naming.*" %>

<%
String sleepString = request.getParameter("sleep");
if( sleepString != null ) {
    int sleepInt = Integer.parseInt(sleepString);
    Thread.sleep(sleepInt);
}

String serverIP = request.getLocalAddr();

DataSource pool;

InitialContext ctx = new InitialContext();
pool = (DataSource)ctx.lookup("java:comp/env/jdbc/TestDB");

Connection conn = null;
Statement  stmt = null;

conn = pool.getConnection();
stmt = conn.createStatement();
ResultSet rset = stmt.executeQuery("SELECT first_name, last_name FROM employees");
int count=0;
%>
<h1>Server IP:  <%=serverIP%></h1>
<h2>Employee List</h2>
<table border=1>
<tr>
    <th>Frist Name</th>
    <th>Last Name</th>
</tr>
<%
while(rset.next()) {
%>
    <tr>
      <td><%=rset.getString("first_name")%></td>
      <td><%=rset.getString("last_name")%></td>
    </tr>

<%
    ++count;
}

if (stmt != null) stmt.close();
if (conn != null) conn.close();

%>
</table>

<h2>Insert a new DB record</h2>
<form action = "insert.jsp" method = "GET">
  First Name: <input type = "text" name = "first"> <br />
  Last Name: <input type = "text" name = "last" /> <br />
  <input type = "submit" value = "Submit" />
</form>

</html>