create table employees (
   id int,
   first_name varchar(50),
   last_name varchar(50),
   primary key (id) );

insert into employees values (1001, 'John', 'Yung');
insert into employees values (1002, 'Frente', 'Pang');
insert into employees values (1003, 'Frank', 'Hui');
